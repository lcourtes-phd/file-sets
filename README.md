File sets for [_Storage Tradeoffs in a Collaborative Backup Service for Mobile Devices_](https://hal.inria.fr/hal-00187069/en)
========================================================================

This repository contains files used by
[chop-eval](https://gitlab.inria.fr/lcourtes-phd/chop-eval) for the
performance evaluation that appears in [_Storage Tradeoffs in a
Collaborative Backup Service for Mobile
Devices_](https://hal.inria.fr/hal-00187069/en).

(Yeah, those files don’t really belong in a Git repo, but by today’s
standards, it’s really not much disk space.)
